## ossi-user 12 SKQ3.210717.001 1631981578155 release-keys
- Manufacturer: oplus   
- Platform: lahaina
- Codename: lahaina
- Brand: oplus
- Flavor: ossi-user
- Release Version: 12
- Id: SKQ3.210717.001
- Incremental: 1631981578155
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Locale: en-US
- Screen Density: undefined
- Fingerprint: oplus/ossi/ossi:12/SKQ3.210717.001/1631981578155:user/release-key     
- OTA version:
- Branch: ossi-user-12-SKQ3.210717.001-1631981578155-release-keys
- Repo: oplus_lahaina_dump


@minati_dumps
